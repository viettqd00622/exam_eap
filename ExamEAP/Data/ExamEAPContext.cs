﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ExamEAP;

namespace ExamEAP.Models
{
    public class ExamEAPContext : DbContext
    {
        public ExamEAPContext(DbContextOptions<ExamEAPContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Currency>().HasData(
                new Currency() { Id = 1, Ten = "USA", Giatri = 23260 },
                 new Currency() { Id = 2, Ten = "EUR", Giatri = 27060 },
                 new Currency() { Id = 3, Ten = "AUD", Giatri = 16798 },
                 new Currency() { Id = 4, Ten = "JPY", Giatri = 20704 }
            );
        }

        public DbSet<ExamEAP.Currency> Currency { get; set; }
    }
}

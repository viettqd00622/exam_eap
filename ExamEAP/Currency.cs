﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamEAP
{
    public class Currency
    {
        public long Id { get; set; }
        public string Ten { get; set; }
        public int Giatri { get; set; }
    }
}
